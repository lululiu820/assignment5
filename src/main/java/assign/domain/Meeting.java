package assign.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "meeting")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "meetings")
public class Meeting {

	@XmlAttribute(name = "id")
	private Long meetingId;

	private String name;
	private Integer year;

	@XmlTransient
	private Project project;

	public Meeting() {
	}

	@Id
	@Column(name = "meeting_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getMeetingId() {
		return meetingId;
	}

	private void setMeetingId(Long meetingId) {
		this.meetingId = meetingId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "year")
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@ManyToOne
	@JoinColumn(name = "project_id")
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
}
