package assign.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "projects")
public class Project {

	@XmlAttribute(name = "id")
	private Long projectId;

	private String name;
	private String description;

	@XmlElementWrapper(name = "meetings")
	private Set<Meeting> meeting;

	public Project() {
	}

	@Id
	@Column(name = "project_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getProjectId() {
		return projectId;
	}

	private void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "project")
	@Cascade({ CascadeType.DELETE })
	public Set<Meeting> getMeetings() {
		return meeting;
	}

	public void setMeetings(Set<Meeting> meetings) {
		this.meeting = meetings;
	}
}