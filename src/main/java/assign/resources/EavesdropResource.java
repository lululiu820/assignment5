package assign.resources;

import java.net.URI;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import assign.domain.Meeting;
import assign.domain.Project;
import assign.services.DBLoader;

@Path("/projects")
public class EavesdropResource {

	DBLoader dbLoader;

	String password;
	String username;
	String dburl;

	public EavesdropResource(@Context ServletContext servletContext) {
		this.dbLoader = new DBLoader();
	}

	@GET
	@Path("/{projectId}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getProject(@PathParam("projectId") String projectIdParam) {
		try {
			Long projectId = Long.parseLong(projectIdParam);

			Project project = dbLoader.getProject(projectId);
			if (project == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}

			return Response.ok(project, MediaType.APPLICATION_XML).build();
		} catch (NumberFormatException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@GET
	@Path("/{projectId}/meetings/{meetingId}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getMeeting(@PathParam("projectId") String projectIdParam,
			@PathParam("meetingId") String meetingIdParam) {

		try {
			Long projectId = Long.parseLong(projectIdParam);
			Long meetingId = Long.parseLong(meetingIdParam);

			Meeting meeting = dbLoader.getMeeting(meetingId);
			if (meeting == null || meeting.getProject().getProjectId() != projectId) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}

			return Response.ok(meeting, MediaType.APPLICATION_XML).build();
		} catch (NumberFormatException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response postProject(Project project) {
		try {
			if (project.getName() == null || project.getName().isEmpty() || project.getDescription() == null
					|| project.getDescription().isEmpty()) {

				return Response.status(Response.Status.BAD_REQUEST).build();
			}

			Long projectId = dbLoader.addProject(project.getName(), project.getDescription());
			return Response.created(new URI("/projects/" + projectId)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@POST
	@Path("/{projectId}/meetings")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response postMeeting(@PathParam("projectId") String projectIdParam, Meeting meeting) {
		try {
			if (meeting.getName() == null || meeting.getName().isEmpty() || meeting.getYear() == null
					|| meeting.getYear() == 0) {

				return Response.status(Response.Status.BAD_REQUEST).build();
			}

			Long projectId = Long.parseLong(projectIdParam);
			Long meetingId = dbLoader.addMeeting(projectId, meeting.getName(), meeting.getYear());
			return Response.created(new URI("/" + projectId + "/meetings/" + meetingId)).build();
		} catch (NumberFormatException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		} catch (NullPointerException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@PUT
	@Path("/{projectId}/meetings/{meetingId}")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response putMeeting(@PathParam("projectId") String projectIdParam,
			@PathParam("meetingId") String meetingIdParam, Meeting meeting) {

		try {
			Long projectId = Long.parseLong(projectIdParam);
			Long meetingId = Long.parseLong(meetingIdParam);

			if (meeting.getName() == null || meeting.getName().isEmpty() || meeting.getYear() == null
					|| meeting.getYear() == 0) {

				return Response.status(Response.Status.BAD_REQUEST).build();
			}

			dbLoader.updateMeeting(projectId, meetingId, meeting.getName(), meeting.getYear());
			return Response.noContent().build();
		} catch (NumberFormatException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		} catch (NullPointerException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@DELETE
	@Path("/{projectId}")
	@Produces(MediaType.APPLICATION_XML)
	public Response deleteProject(@PathParam("projectId") String projectIdParam) {
		try {
			Long projectId = Long.parseLong(projectIdParam);
			dbLoader.deleteProject(projectId);
			return Response.ok().build();
		} catch (NumberFormatException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		} catch (NullPointerException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}