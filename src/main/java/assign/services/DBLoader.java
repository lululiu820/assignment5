package assign.services;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import assign.domain.Meeting;
import assign.domain.Project;

public class DBLoader {
	private SessionFactory sessionFactory;
	private Logger logger;

	@SuppressWarnings("deprecation")
	public DBLoader() {
		sessionFactory = new Configuration().configure().buildSessionFactory();
		logger = Logger.getLogger("EavesdropReader");
	}

	public void loadData(Map<String, List<String>> data) {
		logger.info("Inside loadData.");
	}

	public Project getProject(Long projectId) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Criteria criteria = session.createCriteria(Project.class).add(Restrictions.eq("projectId", projectId));

		@SuppressWarnings("unchecked")
		List<Project> projects = criteria.list();

		if (projects.size() > 0) {
			session.close();
			return projects.get(0);
		} else {
			session.close();
			return null;
		}
	}

	public Meeting getMeeting(Long meetingId) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Criteria criteria = session.createCriteria(Meeting.class).add(Restrictions.eq("meetingId", meetingId));

		@SuppressWarnings("unchecked")
		List<Meeting> meetings = criteria.list();

		if (meetings.size() > 0) {
			session.close();
			return meetings.get(0);
		} else {
			session.close();
			return null;
		}
	}

	public Long addProject(String name, String description) throws Exception {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long projectId = null;

		try {
			tx = session.beginTransaction();
			Project project = new Project();
			project.setName(name);
			project.setDescription(description);
			session.save(project);
			projectId = project.getProjectId();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		} finally {
			session.close();
		}

		return projectId;
	}

	public Long addMeeting(Long projectId, String name, Integer year) throws Exception {
		Project project = getProject(projectId);
		if (project == null) {
			throw new NullPointerException("Project not found for projectId=" + projectId);
		}

		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long meetingId = null;

		try {
			tx = session.beginTransaction();
			Meeting meeting = new Meeting();
			meeting.setProject(project);
			meeting.setName(name);
			meeting.setYear(year);
			session.save(meeting);
			meetingId = meeting.getMeetingId();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		} finally {
			session.close();
		}

		return meetingId;
	}

	public void updateMeeting(Long projectId, Long meetingId, String name, Integer year) throws Exception {
		Meeting meeting = getMeeting(meetingId);
		if (meeting == null || meeting.getProject().getProjectId() != projectId) {
			throw new NullPointerException("Meeting not found for meetingId=" + meetingId);
		}

		Session session = sessionFactory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			meeting.setName(name);
			meeting.setYear(year);
			session.update(meeting);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		} finally {
			session.close();
		}
	}

	public void deleteProject(Long projectId) throws Exception {
		Project project = getProject(projectId);
		if (project == null) {
			throw new NullPointerException("Project not found for projectId=" + projectId);
		}

		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(project);
		session.getTransaction().commit();
		session.close();
	}
}
