package assign.services;

import org.junit.Test;

import assign.domain.Meeting;
import assign.domain.Project;
import junit.framework.TestCase;

public class TestDBLoader extends TestCase {

	DBLoader dbLoader;

	@Override
	protected void setUp() {
		dbLoader = new DBLoader();
	}

	@Test
	public void testAddProject() {
		try {
			Long projectId = dbLoader.addProject("test project", "test add project");
			Project project = dbLoader.getProject(projectId);

			assertNotNull(project);
			assertEquals("test project", project.getName());
			assertEquals("test add project", project.getDescription());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testAddMeeting() {
		try {
			Long projectId = dbLoader.addProject("test project with meeting", "test add meeting");
			Long meetingId = dbLoader.addMeeting(projectId, "test meeting", 1993);
			Project project = dbLoader.getProject(projectId);

			assertNotNull(project);
			assertEquals("test project with meeting", project.getName());
			assertEquals("test add meeting", project.getDescription());
			assertEquals(1, project.getMeetings().size());

			Meeting meeting = dbLoader.getMeeting(meetingId);
			assertEquals("test meeting", meeting.getName());
			assertEquals(new Integer(1993), meeting.getYear());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testUpdateMeeting() {
		try {
			Long projectId = dbLoader.addProject("test project with meeting", "test update meeting");
			Long meetingId = dbLoader.addMeeting(projectId, "test meeting", 1993);
			Project project = dbLoader.getProject(projectId);

			assertNotNull(project);
			assertEquals("test project with meeting", project.getName());
			assertEquals("test update meeting", project.getDescription());
			assertEquals(1, project.getMeetings().size());

			Meeting meeting = dbLoader.getMeeting(meetingId);
			assertEquals("test meeting", meeting.getName());
			assertEquals(new Integer(1993), meeting.getYear());

			dbLoader.updateMeeting(projectId, meetingId, "test meeting updated", 2016);
			Meeting updatedMeeting = dbLoader.getMeeting(meetingId);
			assertEquals("test meeting updated", updatedMeeting.getName());
			assertEquals(new Integer(2016), updatedMeeting.getYear());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDeleteProject() {
		try {
			Long projectId = dbLoader.addProject("test project with meeting", "test delete project");
			Long meetingId = dbLoader.addMeeting(projectId, "test meeting", 1993);
			Project project = dbLoader.getProject(projectId);

			assertNotNull(project);
			assertEquals("test project with meeting", project.getName());
			assertEquals("test delete project", project.getDescription());
			assertEquals(1, project.getMeetings().size());

			Meeting meeting = dbLoader.getMeeting(meetingId);
			assertEquals("test meeting", meeting.getName());
			assertEquals(new Integer(1993), meeting.getYear());

			dbLoader.deleteProject(projectId);
			assertNull(dbLoader.getProject(projectId));
			assertNull(dbLoader.getMeeting(meetingId));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
